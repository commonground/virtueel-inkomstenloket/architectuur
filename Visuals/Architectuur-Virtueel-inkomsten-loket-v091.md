# Architectuur Virtueel inkomsten loket

06\-08\-2022 \- CONCEPT 0.1

F\. de Waal

# Inhoud

Bespreking architectuur

Van pilot naar robuuste oplossing

Samenhang functionele componenten

Ontwikkeling functionele componenten door leveranciers

5 lagen architectuur functionele componenten

Zaakgericht werken

Infra: beveiligde data uitwisseling functionele componenten

Datamodellen administraties en ontologie VIL

Ontwikkelingen in de samenwerking

# Hieronder de visualisatie in vooralsnog jpg formaat

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-0.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-1.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-2.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-3.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-4.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-5.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-6.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-7.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-8.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-9.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-10.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-11.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-12.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-13.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-14jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-15.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-16.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-17.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-18.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-19.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-20.jpg)

![](Visuals/img/8cac86ca6133b1473837986a187ae03b-21.jpg)
