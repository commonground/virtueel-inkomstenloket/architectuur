# Architectuur

Deze folder bevat archimate platen voor het Virtueel inkomensloket

## Laatste stand van zaken

De laatste stand van zaken is zichtbaar gemaakt in Visuals folder. Zie daarvoor ![deze link](Visuals/Architectuur-Virtueel-inkomsten-loket-v091.md).

Het is nog onderwerp van gesprek of deze (nieuwe) visualisaties op termijn worden opgenomen in de modeler modelio. Vooralsnog zijn de instructies daartoe hieronder nog zichtbaar.


## Installatie

De modeler die gebruikt wordt is modelio. Deze kan gedownload en geïnstalleerd worden op:

https://www.modelio.org/downloads/download-modelio.html

Voor archimate moet een aparte plugin geïnstalleerd worden. Deze kun je vinden op: 

https://www.modeliosoft.com/en/downloads/plugins/archimate.html

Voor het exporteren naar HTML
https://www.modeliosoft.com/en/downloads/msoft-modules/modelio-4-modules/modules-for-modelio-4-1-x/404-webmodelpublisher-4-10/file.html

Om deze te installeren ga hiertoe naar menu:

configuration / modules

Klik op `add` en selecteer WebModelPublisher

Klik op `Deploy in the Project`

Het mapje `ViewPoint` bevat nu de export functie voor het publiceren naar de `./doc` directory en kan gecommit worden naar de repo.

## Workspace setup

Start Modelio op en kies voor menu: File / Switch Workspace 

Kies de folder van deze repo ~/doc/Architecture/Workspace

De workspace voor het virtueel inkomensloket kan nu geopend worden en is daarmee onder source beheer via Git.

**Sluit** modelio voordat je pushed/pulled van en naar Git in verband met file locking.
In principe heeft modelio in de commerciële versie een git plugin, we moeten nog goed evalueren of de methode middels .gitignore op de /.runtime folder in de modelio workspace zich naar behoren gedraagd wanneer meerdere mensen commits doen op de archimate modellen. Met name in het kader van merge requests.

Het verwijzen naar de Archimate ViewPoint plaatjes kan eenvoudig binnen markdown.

Bijvoorbeeld:

```
![Zaakgericht Werken](./Workspace/Virtueel%20Inkomensloket/doc/ViewPoints/img/bd1c2de3-e084-4d8e-a8fd-3ec58d1087c2.png
```

Resulteert in:

![Zaakgericht Werken](./Workspace/Virtueel%20Inkomensloket/doc/ViewPoints/img/bd1c2de3-e084-4d8e-a8fd-3ec58d1087c2.png)

Helaas genereert de HTML exporter zelf een unieke, niet vriendelijke, naam van het plaatje.
